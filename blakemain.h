#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include "blake.h"
#include "common.h"

using namespace std;

class Blakemain{
protected:
	Blake*blake;
	Common*common;
	unsigned char buffer[64];
	unsigned char hash[32];

public:
	Blakemain(Blake*blake,Common*common);
	~Blakemain();
	void write_short();
	void write_long();	
	void hash_message(char*message,int message_length);
	void hash_file(char*file_name);
	void main(char**argv);
};
