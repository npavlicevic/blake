#include "blake.h"

using namespace std;

unsigned int Blake::p[14][16]={
	{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
	{14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3},
	{11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4},
	{7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8},
	{9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13},
	{2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9},
	{12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11},
	{13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10},
	{6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5},
	{10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0},
	{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
	{14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3},
	{11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4},
	{7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8}
};

const unsigned int Blake::block_size_bytes=64;

unsigned int Blake::cons[16]={
	0x243F6A88, 0x85A308D3, 0x13198A2E, 0x03707344,
	0xA4093822, 0x299F31D0, 0x082EFA98, 0xEC4E6C89,
	0x452821E6, 0x38D01377, 0xBE5466CF, 0x34E90C6C,
	0xC0AC29B7, 0xC97C50DD, 0x3F84D5B5, 0xB5470917
};

unsigned int Blake::s[4]={0,0,0,0};

Blake::Blake()
{
	this->h[0]=0x6A09E667;
	this->h[1]=0xBB67AE85;
	this->h[2]=0x3C6EF372;
	this->h[3]=0xA54FF53A;
	this->h[4]=0x510E527F;
	this->h[5]=0x9B05688C;
	this->h[6]=0x1F83D9AB;
	this->h[7]=0x5BE0CD19;

	this->message_length_bits=this->t=0;
}

Blake::~Blake(){}

void Blake::get_word(unsigned char * block)
{
	for(int i=0;i<64;i+=4)
	{
		this->word[i>>2]=(((unsigned int)block[i])<<24) | (((unsigned int)block[i+1])<<16) | (((unsigned int)block[i+2])<<8) | ((unsigned int)block[i+3]);
	}
}

unsigned int Blake::rotate_right(unsigned int x,unsigned int p)
{
	unsigned int wlength=sizeof(x)<<3;

	return ((x>>p)|x<<(wlength-p));
}

void Blake::block_operate(unsigned char * block)
{
	this->get_word(block);

	#define g(a,b,c,d,k)\
			this->v[a]=(unsigned int)(this->v[a]+this->v[b])+((unsigned int)(this->word[p[i][k<<1]]^cons[p[i][(k<<1)+1]]));\
			this->v[d]=this->rotate_right((unsigned int)(this->v[d]^this->v[a]),16);\
			this->v[c]=(unsigned int)(this->v[c]+this->v[d]);\
			this->v[b]=this->rotate_right((unsigned int)(this->v[b]^this->v[c]),12);\
			this->v[a]=(unsigned int)(this->v[a]+this->v[b])+((unsigned int)(this->word[p[i][(k<<1)+1]]^cons[p[i][k<<1]]));\
			this->v[d]=this->rotate_right((unsigned int)(this->v[d]^this->v[a]),8);\
			this->v[c]=(unsigned int)(this->v[c]+this->v[d]);\
			this->v[b]=this->rotate_right((unsigned int)(this->v[b]^this->v[c]),7);

	this->t=this->message_length_bits;

	memcpy(this->v,this->h,sizeof(unsigned int)<<3);
	this->v[8]=s[0]^cons[0];
	this->v[9]=s[1]^cons[1];
	this->v[10]=s[2]^cons[2];
	this->v[11]=s[3]^cons[3];
	this->v[12]=((unsigned int)this->t)^cons[4];
	this->v[13]=((unsigned int)this->t)^cons[5];
	this->v[14]=((unsigned int)(this->t>>32))^cons[6];
	this->v[15]=((unsigned int)(this->t>>32))^cons[7];

	for(int i=0;i<14;i++)
	{
		g(0,4,8,12,0);
		g(1,5,9,13,1);
		g(2,6,10,14,2);
		g(3,7,11,15,3);
		g(0,5,10,15,4);
		g(1,6,11,12,5);
		g(2,7,8,13,6);
		g(3,4,9,14,7);
	}

	this->h[0]=this->h[0]^s[0]^this->v[0]^this->v[8];
	this->h[1]=this->h[1]^s[1]^this->v[1]^this->v[9];
	this->h[2]=this->h[2]^s[2]^this->v[2]^this->v[10];
	this->h[3]=this->h[3]^s[3]^this->v[3]^this->v[11];
	this->h[4]=this->h[4]^s[0]^this->v[4]^this->v[12];
	this->h[5]=this->h[5]^s[1]^this->v[5]^this->v[13];
	this->h[6]=this->h[6]^s[2]^this->v[6]^this->v[14];
	this->h[7]=this->h[7]^s[3]^this->v[7]^this->v[15];
}

void Blake::update_hash_block(unsigned char * input,unsigned int input_len_bytes)
{
	this->message_length_bits+=(input_len_bytes<<3);

	this->block_operate(input);
}

void Blake::final_hash_block(unsigned char * input, unsigned int input_len_bytes)
{
	unsigned int block_size_temp_bytes=block_size_bytes;

	this->message_length_bits+=(input_len_bytes<<3);

	if(input_len_bytes==55)
	{
		input[input_len_bytes]=0x81;
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits>>56);
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits>>48);
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits>>40);
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits>>32);
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits>>24);
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits>>16);
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits>>8);
		input[++input_len_bytes]=(unsigned char)(this->message_length_bits);

		this->block_operate(input);
	}
	else if(input_len_bytes<55)
	{
		input[input_len_bytes++]=0x80;

		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits);
		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>8);
		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>16);
		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>24);
		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>32);
		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>40);
		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>48);
		input[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>56);
		input[--block_size_temp_bytes]=0x01;
		
		memset(input+input_len_bytes,0,block_size_temp_bytes-input_len_bytes);

		this->block_operate(input);
	}
	else
	{
		input[input_len_bytes++]=0x80;

		memset(input+input_len_bytes,0,block_size_bytes-input_len_bytes);

		this->block_operate(input);

		unsigned char buffer[block_size_bytes];

		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits);
		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>8);
		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>16);
		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>24);
		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>32);
		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>40);
		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>48);
		buffer[--block_size_temp_bytes]=(unsigned char)(this->message_length_bits>>56);
		buffer[--block_size_temp_bytes]=0x01;

		this->block_operate(buffer);
	}
}

void Blake::get_hash_bytes(unsigned char * result)
{
	for(int i=0;i<32;i+=4)
	{
		result[i]=(unsigned char)(this->h[i>>2]>>24);
		result[i+1]=(unsigned char)(this->h[i>>2]>>16);
		result[i+2]=(unsigned char)(this->h[i>>2]>>8);
		result[i+3]=(unsigned char)(this->h[i>>2]);
	}
}

unsigned int Blake::get_block_size_bytes(){
	return block_size_bytes;
}
