#include <iostream>
#include <cstring>

class Blake
{
private:
	unsigned int word[16];//size in words
	static unsigned int p[14][16];
	unsigned long long t;
	unsigned int v[16];//size in words
	unsigned int h[8];//size in words
	static const unsigned int block_size_bytes;
	static unsigned int s[4];//size in words
	static unsigned int cons[16];//size in words

	unsigned long long message_length_bits;

	void get_word(unsigned char * block);
	unsigned int rotate_right(unsigned int x,unsigned int p);
	void block_operate(unsigned char * block);

public:
	Blake();
	~Blake();

	void update_hash_block(unsigned char * input,unsigned int input_len_bytes);
	void final_hash_block(unsigned char * input, unsigned int input_len_bytes);
	void get_hash_bytes(unsigned char*result);
	unsigned int get_block_size_bytes();
};
