#include <stdio.h>
#include "common.h"

void Common::show_hex(const unsigned char*array,
	int length){

	while(length--){
		printf("%.02x",*array++);
	}

	printf("\n");
}

void Common::reset_file(const char*file_name){
	FILE*file;
	file=fopen(file_name,"wb");
	fclose(file);
}