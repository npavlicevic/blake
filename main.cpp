#include "blakemain.h"

int main(int argc,char**argv)
{
	Blake*blake=new Blake();
	Common*common=new Common();
	Blakemain*blakemain=new Blakemain(blake,common);

	blakemain->main(argv);

	delete blakemain;

	return 0;
}
