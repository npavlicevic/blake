#include "blakemain.h"

Blakemain::Blakemain(Blake*blake,Common*common){
	this->blake=blake;
	this->common=common;
}

Blakemain::~Blakemain(){
	delete this->blake;
	delete this->common;
}

void Blakemain::write_short(){
	this->buffer[0]=0;

	this->blake->final_hash_block(this->buffer,1);
	this->blake->get_hash_bytes(this->hash);
	cout<<"Message hash"<<endl<<endl;
	this->common->show_hex(this->hash,32);
}

void Blakemain::write_long(){
	memset(this->buffer,0,64);

	this->blake->update_hash_block(this->buffer,64);
	this->blake->final_hash_block(this->buffer,8);
	this->blake->get_hash_bytes(this->hash);
	cout<<"Message hash"<<endl<<endl;
	this->common->show_hex(hash,32);
}

void Blakemain::hash_message(char*message,int message_length){
		if(message_length<=64){
			memcpy(this->buffer,message,message_length);

			this->blake->final_hash_block(
				this->buffer,
				message_length
			);
			this->blake->get_hash_bytes(this->hash);
			cout<<"Message hash"<<endl<<endl;
			this->common->show_hex(this->hash,32);
		}
}

void Blakemain::hash_file(char*file_name){
	unsigned int block_size_bytes=this->blake->get_block_size_bytes();
	unsigned int bytes_read;
	ifstream file(file_name,ifstream::binary);

	long int start_time;
	long int time_difference;
	struct timespec gettime_now;

	clock_gettime(CLOCK_REALTIME,&gettime_now);
	start_time=gettime_now.tv_nsec;

	while((bytes_read=
		file.read((char*)buffer,block_size_bytes).gcount())==
		block_size_bytes){
			this->blake->update_hash_block(this->buffer,bytes_read);
		}

	this->blake->final_hash_block(this->buffer,bytes_read);

	clock_gettime(CLOCK_REALTIME,&gettime_now);
	time_difference=gettime_now.tv_nsec-start_time;

	cout<<"Time difference"<<endl<<endl;
	cout<<time_difference<<endl<<endl;

	this->blake->get_hash_bytes(this->hash);
	cout<<"Message hash"<<endl<<endl;
	this->common->show_hex(this->hash,32);

	file.close();
}

void Blakemain::main(char**argv){
	if(!strcmp(argv[1],"-ws")){
		this->write_short();
	}else if(!strcmp(argv[1],"-wl")){
		this->write_long();
	}else if(!strcmp(argv[1],"-h")){
		this->hash_message(argv[2],atoi(argv[3]));
	}else if(!strcmp(argv[1],"-f")){
		this->hash_file(argv[2]);
	}else if(!strcmp(argv[1],"-u")){
		cout<<"blake -ws to hash short message"<<endl<<endl<<endl;
		cout<<"blake -wl to hash long message"<<endl<<endl<<endl;
		cout<<"blake -h <message> <message_length> to hash a message"<<endl<<endl<<endl;
		cout<<"blake -f <file_name> to hash a file"<<endl<<endl<<endl;
	}
}
